#autorzy Maciej Falkiewicz i Maciej Sochalski
#Rozwiazanie jest oparte o algorytm bayes'a ktory za posrednictwem objawow jest w stanie wyliczyc prawdopodobienstwo
#zachorowania na COVID'a, grype badz jakakolwiek inna chorobe(Zestaw chorob mozna rozszerzyc wedle zachcianki tak samo
#wspolczynniki ktore maja wplyw na prawdopodobienstwo zachorowania na wskazana chorobe.

import yaml, os, sys, math
#otwieranie pliku yaml, w ktorym mamy Hipotezy i fakty
filename = open('choroba.yaml', 'r', encoding='UTF8')
data=yaml.safe_load(filename)

print("Hipoteza, prawdopodobieństwo a priori:")
for h in data["Hypotheses"]:
    print("{}, {}".format(h["name"], h["prob"]))

print()
print("POJEDYNCZE FAKTY")

#Liczenie prawdopodobienstwa faktow
Pr_f = []
for fact in data["Facts"]:
    sum = 0
    for index, h in enumerate(data["Hypotheses"]):
        sum = sum + h["prob"] * fact["prob"][index]
    Pr_f.append([fact["name"], round(sum, 5)])

print("Fakt, prawdopodobieństwo:")
for x in Pr_f:
    print(*x, sep=", ")

# Liczenie prawdopodobienstwa hipotez pojedynczych faktow
Pr_h_f = []
for indexh, h in enumerate(data["Hypotheses"]):
    for indexf, fact in enumerate(data["Facts"]):
        pr = round(h["prob"] * fact["prob"][indexh] / Pr_f[indexf][1], 5)
        Pr_h_f.append([h["name"], fact["name"], pr])


print()
print("Hipoteza, fakt, prawdopodobieństwo:")
for x in Pr_h_f:
    print(*x, sep=', ')

print()
print("KILKA FAKTÓW JEDNOCZEŚNIE")
print("Fakty:")
for index, fact in enumerate(data["Facts"]):
    print(index, fact["name"])

# Czytanie listy faktow
ok = False
while not ok:
    selected_facts_indexes = input("Podaj numery objawów, które masz oddzielając je spacjami: ").split()
    # przerzucenie na inta
    selected_facts_indexes = [int(i) for i in selected_facts_indexes]
    # usuniecie duplikatow
    selected_facts_indexes = list(dict.fromkeys(selected_facts_indexes))
    # Sprawdzenie czy liczby sa mozliwe do uzycia
    ok = set(selected_facts_indexes).issubset(range(len(data["Facts"])))

selected_facts = [data["Facts"][int(i)]["name"] for i in selected_facts_indexes]

# Wyliczenie prawdopodobienstwa wskazanych faktow
denumerator = 0
for ih, h in enumerate(data["Hypotheses"]):
    for index, fact in enumerate(data["Facts"]):
        #zbieram prawdopodobienstwa faktow do listy
        selected_facts_val = [data["Facts"][i]["prob"][ih] for i in selected_facts_indexes]
        # math.prod = sluzy do obliczenia wszystkich elementow obecnych w danej iterowanej
        selected_facts_val_prod = math.prod(selected_facts_val)
    denumerator = denumerator + selected_facts_val_prod * h["prob"]

#Prawie Takie samo jak denumerator
for ih, h in enumerate(data["Hypotheses"]):
    for index, fact in enumerate(data["Facts"]):
        selected_facts_val = [data["Facts"][i]["prob"][ih] for i in selected_facts_indexes]
        selected_facts_val_prod = math.prod(selected_facts_val)

    numerator = selected_facts_val_prod * h["prob"]
    # Wyswietlenie koncowego wyniku.
    pr = round(numerator / denumerator, 5)
    result = round(pr*100,5)
    print("Prawdopodobieństwo hipotezy {} przy uwzględnieniu faktów ({}) wynosi: {}%." \
          .format(h["name"], ', '.join(selected_facts), result))