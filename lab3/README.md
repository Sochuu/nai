## Ćwiczenie 3 - program wyliczający prawdopodobieństwo zachorowania na Covid-19
* [Kod (plik py)](https://gitlab.com/Sochuu/nai/-/blob/master/lab3/covid.py)
* [Kod (plik yaml)](https://gitlab.com/Sochuu/nai/-/blob/master/lab3/choroba.yaml)
* [Zrzuty ekranu](https://gitlab.com/Sochuu/nai/-/tree/master/lab3/screens)