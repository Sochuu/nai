# Source of the puzzle: https://www.codingame.com/ide/puzzle/temperatures
# Author of the solution: Maciej Sochalski

import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

n = int(input())  # the number of temperatures to analyse
t_close = 5526
if n > 0:
    for i in input().split():
        # t: a temperature expressed as an integer ranging from -273 to 5526
        t = int(i)
        if (abs(t_close) > abs(t)):
            t_close = t
        elif (abs(t_close) == abs(t)):
            if (t_close <= t):
                t_close = t

    # Write an answer using print
    # To debug: print("Debug messages...", file=sys.stderr, flush=True)

    print(t_close)
else:
    print(0)
