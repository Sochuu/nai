#author -> Maciej Falkiewicz and Maciej Sochalski

import pandas as pd
import numpy as np
from sklearn.neighbors import NearestNeighbors


#reading data and creating matrix
data = pd.read_csv('people_movies.csv', sep=';')
df = pd.DataFrame(data)
data_top = df.head(10)
user_movies = df.pivot_table(index=['userID'], columns=['movieID'], values='mark').reset_index(drop=True)
user_movies.fillna(0, inplace=True)
user_movies=pd.DataFrame(user_movies)
user_movies.head()
print(user_movies)


def find_neighborhood(userID, n):
    """
    This method is for finding neighborhood for our user. It uses Pearson's correlation ratio
    :param userID:
    :param n:
    :return: User who is the most similar to him and value of similarity in range <-1,1>
    """
    model_knn = NearestNeighbors(metric="correlation", algorithm="brute")
    model_knn.fit(user_movies)
    distances, indices = model_knn.kneighbors(user_movies.iloc[userID - 1, :].values.reshape(1, -1), n_neighbors=n + 1)
    similarities = 1 - distances.flatten()
    print("\n{0} most similar users for user with id {1}:\n".format(n, userID))

    for i in range(0, len(indices.flatten())):
        if indices.flatten()[i]+1 == userID:
            continue;
        else:
            print("{0}: User {1}, with similarity of {2}".format(i, indices.flatten()[i]+1, similarities.flatten()[i]))
    return similarities, indices


# find_neighborhood(1,1)

def find_movie_to_recommend(userID, n):
    """
    This method is used to find movies for user that he could possibly like
    :param userID:
    :param n:
    :return: List of 6 movies.
    """
    similarities, indices = find_neighborhood(userID, n)
    movies_to_recommendation = []
    for itemID in range(1, max(user_movies)):
        neighborhood_ratings = []
        for i in range(0, len(indices.flatten())):
            if indices.flatten()[i]+1 == userID:
                continue;
            else:
                neighborhood_ratings.append(user_movies.iloc[indices.flatten()[i], itemID-1])
        weights = np.delete(indices.flatten(), 0)
        prediction = round((neighborhood_ratings * weights).sum() / weights.sum())
        if prediction > 5:
            movies_to_recommendation.append(itemID)
    top_6_idx = np.argsort(movies_to_recommendation)[-6:]
    top_6_values = [[movies_to_recommendation[i] for i in top_6_idx]]
    print("moviesID to recommand:  {}".format(top_6_values))

find_movie_to_recommend(1,3)

def find_movie_not_to_recommend(userID, n):
    """
    This method is used to find movies for user that he should never try watching
    :param userID:
    :param n:
    :return:
    """
    similarities, indices = find_neighborhood(userID, n)
    movies_that_suck = []
    for itemID in range(1, max(user_movies)):
        neighborhood_ratings = []
        for i in range(0, len(indices.flatten())):
            if indices.flatten()[i]+1 == userID:
                continue;
            else:
                neighborhood_ratings.append(user_movies.iloc[indices.flatten()[i], itemID-1])
        weights = np.delete(indices.flatten(), 0)
        prediction = round((neighborhood_ratings * weights).sum() / weights.sum())
        if prediction > 1:
            movies_that_suck.append(itemID)
    top_6_idx = np.argsort(movies_that_suck)[-6:]
    top_6_values =[[movies_that_suck[i]] for i in top_6_idx]
    print("moviesID to not recommand:  {}".format(top_6_values))

find_movie_not_to_recommend(9,3)