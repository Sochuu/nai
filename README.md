## NAI - Narzędzia Sztucznej Inteligencji
* [Exercise 1 - puzzles](https://gitlab.com/Sochuu/nai/-/tree/master/lab1)
* [Exercise 2 - game](https://gitlab.com/Sochuu/nai/-/tree/master/lab2)
* [Exercise 3 - fuzzy logic program](https://gitlab.com/Sochuu/nai/-/tree/master/lab3)
* [Exercise 4 - movie recommendation system](https://gitlab.com/Sochuu/nai/-/tree/master/lab4)
* [Exercise 5 - SVM classification program](https://gitlab.com/Sochuu/nai/-/tree/master/lab5)
* [Exercise 6 - neural network program](https://gitlab.com/Sochuu/nai/-/tree/master/lab6)
