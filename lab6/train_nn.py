#Authors: Maciek Falkiewicz and Maciej Sochalski
import tensorflow as tf


class FullyConnectedForClassification:
    def build(number_of_neurons, classes):
        #Initialize the model
        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.Flatten())
        model.add(tf.keras.layers.Dense(number_of_neurons, activation='relu'))
        model.add(tf.keras.layers.Dense(classes, activation="softmax"))
        return model


class LeNet5:
    def build(width, height, depth, classes):
        # Initialize the model
        model = tf.keras.models.Sequential()
        inputShape = (height, width, depth)

        model.add(tf.keras.layers.Conv2D(6, kernel_size=(5, 5), strides=(1, 1),
                                      activation='tanh', input_shape=inputShape, padding='same'))
        model.add(tf.keras.layers.AveragePooling2D(pool_size=(2, 2), strides=(2, 2),
                                                   padding='valid'))
        model.add(tf.keras.layers.Conv2D(16, kernel_size=(5, 5), strides=(1, 1), activation='tanh',
                                         padding='valid'))
        model.add(tf.keras.layers.AveragePooling2D(pool_size=(2, 2), strides=(2, 2),
                                                   padding='valid') )
        model.add(tf.keras.layers.Conv2D(120, kernel_size=(5, 5), strides=(1, 1),
                                         activation='tanh', padding='valid'))
        model.add(tf.keras.layers.Flatten())
        model.add(tf.keras.layers.Dense(84, activation='tanh'))
        model.add(tf.keras.layers.Dense(classes, activation='softmax'))
        return model
