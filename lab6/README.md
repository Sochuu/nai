# Exercise 6 - Neural Network

* To run program you have to install:
  * TensorFlow
  * imutils
  * opencv-python
  * NumPy
  * pandas
  * matplotlib
  ```
  pip install {package}
  ```
  <br />

* There are 3 problems solved in training_animals file:
  * Teaching model via .csv file from wine dataset
  * Teaching model via jpg/png files from animals dataset
  * Teaching model via tensorflow dataset
<br /><br />

* If you run animals dataset, you have to type input dataset path in that case: data/images
* And then run test_animals file on testing files but again you have to pass him path to images you are going to test on your trained network: data/images_test
<br /><br />

* #### Run program:
  ```
  python train_animals.py -m {model name} -e {number_of_epochs} -p {choose from: 'wine','animals','clothes'}
  ```
  <br />

* ## Example:
  ```
  python train_animals.py -m test -e 10 -p animals -d data/images
  ```
  and then (only for animals dataset):
  ```
  python test_animals.py -m test -t data/images_test
  ```