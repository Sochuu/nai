#Authors: Maciek Falkiewicz and Maciej Sochalski
import os

import tensorflow as tf
import train_nn
import argparse
import pickle
from imutils import paths
import cv2
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


BATCH_SIZE_VALUE = 32
ClASSES = 10
EPOCH = 100
VALIDATION_SLIT_VALUE = 0.2
MODEL = "model"
NUMBER_OF_NEURONS = 100
PROBLEM = "wine"
IMG_DEPTH = 3
LOSS ='categorical_crossentropy'
OPTIMIZER = 'nadam'


'''Parameters to load from console'''
ap = argparse.ArgumentParser()
ap.add_argument("-e", "--epochs", required=False, type=int, help="number of epochs",
                default=EPOCH, metavar='int')
ap.add_argument("-m", "--model", required=False, help="output model file name (extension .h5 will be added)",
                default=MODEL, metavar="filename")
ap.add_argument("-d", "--dataset", required=False,
                help="Path to input dataset ~ only with animals case and then it's required")
ap.add_argument("-p", "--problem", required=False, choices=['animals', 'wine', 'clothes'],
                help="Choose problem to solve", default=PROBLEM)

args = vars(ap.parse_args())
EPOCH = args["epochs"]
MODEL = args["model"]
DATASET = args['dataset']
PROBLEM = args['problem']
DO_RESIZE = True
'''
With higher RESIZE, the accuracy of result is increasing, but it takes obviously more time.
'''
RESIZE_X=32
RESIZE_Y=32

'''
Datasets were downloaded via Bulk Image Downloader
Free version allows only to download 100 images per session
So if you need more than 100 you need to struggle a little
In my case each of datasets contains around 300 images.
'''
if PROBLEM =='wine':
    white_wine = pd.read_csv("./data/winequality-white.csv", delimiter=';', header=0)
    red_wine = pd.read_csv("./data/winequality-red.csv", delimiter=';', header=0)
    y_train = white_wine.iloc[:, 11] #y_train
    x_train = white_wine.iloc[:, :11] # x_train

    y_test = red_wine.iloc[:, 11]
    x_test = red_wine.iloc[:, :11]

    print("Model is compiling...")

    model_wine = train_nn.FullyConnectedForClassification.build(NUMBER_OF_NEURONS, ClASSES)
    # model.summary()
    model_wine.compile(loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True), optimizer=OPTIMIZER, metrics=['accuracy'])
    print("Training network..")

    model_history = model_wine.fit(x=x_train, y=y_train, batch_size=BATCH_SIZE_VALUE,
                  epochs=EPOCH)
    print("Saving model...")
    model_wine.save(MODEL + ".h5")
    test_loss, test_acc = model_wine.evaluate(x_test, y_test, verbose=2)
    print('\nTest accuracy:', test_acc)
    probability_model = tf.keras.Sequential([model_wine, tf.keras.layers.Softmax()])
    # Predicting via learnt model
    predictions = probability_model.predict(x_test)

    pd.DataFrame(model_history.history).plot()
    plt.grid(True)
    plt.gca().set_ylim(0, 2)
    plt.show()

elif PROBLEM == 'animals':
    print("Loading images ....")
    data = []
    labels = []
    labels_text = []
    imagePaths = paths.list_images(args['dataset'])
    for imagePath in imagePaths:
        image = cv2.imread(imagePath)
        if DO_RESIZE:
            image = cv2.resize(image, (RESIZE_X, RESIZE_Y))
        image = tf.keras.preprocessing.image.img_to_array(image)
        dirname = imagePath.split(os.path.sep)[-2]
        dirname_list = dirname.split("-")
        label = int(dirname_list[0])
        try:
            label_text = dirname_list[1]
        except KeyError:
            label_text = int(dirname_list[0])

        data.append(image)
        labels.append(label)
        labels_text.append(label_text)

    classes = np.unique(labels_text)

    f = open(MODEL + ".lbl", "wb")
    f.write(pickle.dumps(classes))
    f.close()

    labels = np.array(labels)

    no_classes = len(classes)

    data = np.array(data, dtype="float") / 255.0

    train_data = data
    train_labels = labels

    train_labels = tf.keras.utils.to_categorical(train_labels, num_classes=no_classes)

    model_animals = train_nn.LeNet5.build(width=RESIZE_X, height=RESIZE_Y, depth=IMG_DEPTH, classes=no_classes)
    model_animals.summary()
    model_animals.compile(loss=LOSS, optimizer=OPTIMIZER,
                  metrics=['accuracy'])

    print("Training network..")

    h = model_animals.fit(x=train_data, y=train_labels, batch_size=BATCH_SIZE_VALUE,
                  validation_split=VALIDATION_SLIT_VALUE, epochs=EPOCH, verbose=1)
    print("Saving model...")
    model_animals.save(MODEL + ".h5")

elif PROBLEM == 'clothes':
    fashion_mnist = tf.keras.datasets.fashion_mnist
    '''
    Load the data from dataset
    train_images and train_labels for training the model
    test_images and test_labels for testing the model
    '''
    (train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()
    '''Add classnames'''
    class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
                   'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

    '''Preprocess the data'''
    plt.figure()
    plt.imshow(train_images[0])
    plt.colorbar()
    plt.grid(False)
    plt.show()
    '''Scale values train_images and test_images to a range of 0 to 1'''
    train_images = train_images / 255.0
    test_images = test_images / 255.0

    plt.figure(figsize=(10, 10))
    for i in range(25):
        '''
        Displays the first 25 images from the training set
        '''
        plt.subplot(5, 5, i + 1)
        plt.xticks([])
        plt.yticks([])
        plt.grid(False)
        plt.imshow(train_images[i], cmap=plt.cm.binary)
        plt.xlabel(class_names[train_labels[i]])
    plt.show()
    '''Set up the layers'''
    model = tf.keras.Sequential([
        tf.keras.layers.Flatten(input_shape=(28, 28)),
        tf.keras.layers.Dense(128, activation='relu'),
        tf.keras.layers.Dense(10)
    ])
    #Compile the model
    model.compile(optimizer='adam',
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])
    print("Training model....")
    model.fit(train_images, train_labels, epochs=10)

    test_loss, test_acc = model.evaluate(test_images, test_labels, verbose=2)

    print('\nTest accuracy:', test_acc)

    probability_model = tf.keras.Sequential([model,
                                             tf.keras.layers.Softmax()])
    predictions = probability_model.predict(test_images)

# Graphing to look at the full set of 10 class predictions
def plot_image(i, predictions_array, true_label, img):
    true_label, img = true_label[i], img[i]
    plt.grid(False)
    plt.xticks([])
    plt.yticks([])

    plt.imshow(img, cmap=plt.cm.binary)

    predicted_label = np.argmax(predictions_array)
    if predicted_label == true_label:
        color = 'blue'
    else:
        color = 'red'

    plt.xlabel("{} {:2.0f}% ({})".format(class_names[predicted_label],
                                         100 * np.max(predictions_array),
                                         class_names[true_label]),
               color=color)


def plot_value_array(i, predictions_array, true_label):
    true_label = true_label[i]
    plt.grid(False)
    plt.xticks(range(10))
    plt.yticks([])
    thisplot = plt.bar(range(10), predictions_array, color="#777777")
    plt.ylim([0, 1])
    predicted_label = np.argmax(predictions_array)

    thisplot[predicted_label].set_color('red')
    thisplot[true_label].set_color('blue')


    # Plot the first X test images, their predicted labels, and the true labels.
    # Color correct predictions in blue and incorrect predictions in red.
    num_rows = 5
    num_cols = 3
    num_images = num_rows * num_cols
    plt.figure(figsize=(2 * 2 * num_cols, 2 * num_rows))
    for i in range(num_images):
        plt.subplot(num_rows, 2 * num_cols, 2 * i + 1)
        plot_image(i, predictions[i], test_labels, test_images)
        plt.subplot(num_rows, 2 * num_cols, 2 * i + 2)
        plot_value_array(i, predictions[i], test_labels)
    plt.tight_layout()
    plt.show()
