#Authors: Maciek Falkiewicz and Maciej Sochalski
import os

import tensorflow as tf
import numpy as np
import argparse
import cv2
import pickle


DO_RESIZE = True
'''Values of RESIZE_X and RESIZE_Y must match to trained values'''
RESIZE_X = 32
RESIZE_Y = 32
# You can set model name by yourself and direct path to test image via console
ap = argparse.ArgumentParser()
ap.add_argument("-m", "--model", required=True,
                help="trained model file name (without the .h5 extension)",
                metavar="filename")
ap.add_argument('-t', '--testset', required=True, help='Path to test images')

args = vars(ap.parse_args())
print("Loading labels")
f = open(args["model"]+".lbl", 'rb')
CLASS_LABELS = pickle.load(f)
f.close()
print("Loading network...")
model = tf.keras.models.load_model(args["model"]+".h5")
print("Classyfing...")
for image_name in os.listdir(args["testset"]):
    image = cv2.imread(args["testset"]+os.path.sep+image_name)

    orig_image = image.copy()
    if DO_RESIZE:
        image = cv2.resize(image, (RESIZE_X,RESIZE_Y))
    '''normalize image'''
    image = image.astype("float")/255.0
    '''preprocessing image'''
    image = tf.keras.preprocessing.image.img_to_array(image)
    image = np.expand_dims(image, axis=0)
    '''prediction on first element of testset'''
    prediction = list(model.predict(image)[0])
    winner_class = prediction.index(max(prediction))
    winner_probability = round(max(prediction) * 100, 3)
    label = "{}: {:.2f}%".format(CLASS_LABELS[winner_class], winner_probability)
    print(label)
    '''Possiblity to see images and probability via code below'''
    # output_image = cv2.resize(orig_image, (600, 600))
    # cv2.putText(output_image, label, (20, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)
    #
    # cv2.imshow("Output", output_image)
    # cv2.waitKey(0)
