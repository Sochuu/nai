#authors : Maciej Falkiewicz and Maciej Sochalski

import pandas as pd
from sklearn import svm

'''
data is available to download from http://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/
'''
white_wine = pd.read_csv("./data/winequality-white.csv", delimiter=';', header=0)
red_wine = pd.read_csv("./data/winequality-red.csv", delimiter=';', header=0)


y_train = white_wine.iloc[:, 11]
X_train = white_wine.iloc[:, :11]

y_test = red_wine.iloc[:, 11]
X_test = red_wine.iloc[:, :11]

#Teaching model
model = svm.SVC(kernel='rbf', gamma=1, C=1, degree=1).fit(X_train, y_train)
#Predicting via learnt model
predict = model.predict(X_test)
#Accuracy of prediction
test_score_rbf = round(model.score(X_test, y_test)*100, 4)

print("Accuracy of model result : {}%".format(test_score_rbf))