## Exercise 2 - Connect 4 game
* [Code](https://gitlab.com/Sochuu/nai/-/blob/master/lab2/main.py)
* [Screens from the game](https://gitlab.com/Sochuu/nai/-/tree/master/lab2/screens)
* [Game rules](https://pl.wikipedia.org/wiki/Czwórki)
* Prepartion Instruction:
    * Install pip:
    ```
    py -m pip install
    ```
    * Install easyAI:
    ```
    py -m pip install easyAI
    ```